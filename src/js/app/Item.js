import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Item extends Component {
  render() {
    const {
      color,
    } = this.props;

    const style = { backgroundColor: color };

    return (
      <div className='hero_item' style={ style } />
    );
  }
}

Item.propTypes = {
  color: PropTypes.string,
};

Item.defaultProps = {
  color: '#fff',
};

export default Item;
