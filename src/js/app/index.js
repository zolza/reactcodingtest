import React, { Component } from 'react';
import Modal from 'react-modal';
import Item from './Item';

const customStyles = {
  content: {
    height: '100px',
    width: '200px',
    textAlign: 'center',
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

class App extends Component {
  state = {
    modalIsOpen: false,
    firstColor: null,
    secondColor: null,
    thirdColor: null,
  }

  openModal = this.openModal.bind(this);
  closeModal = this.closeModal.bind(this);
  afterOpenModal = this.afterOpenModal.bind(this);
  pickColor = this.pickColor.bind(this);
  runSlots = this.runSlots.bind(this);
  checkWin = this.checkWin.bind(this);
  handleClick = this.handleClick.bind(this);

  openModal() {
    this.setState({
      modalIsOpen: true,
    });
  }

  afterOpenModal() {
    this.subtitle.style.color = '#f00';
  }

  closeModal() {
    this.setState({
      modalIsOpen: false,
    });
  }

  pickColor() {
    const colors = ['red', 'blue', 'green', 'yellow'];

    return colors[Math.floor(Math.random() * colors.length)];
  }

  runSlots() {
    const {
      pickColor,
    } = this;

    const firstColor = pickColor();
    const secondColor = pickColor();
    const thirdColor = pickColor();

    const newSlots = {
      firstColor,
      secondColor,
      thirdColor,
    };

    this.setState(newSlots);

    return newSlots;

  }

  handleClick() {
    const {
      runSlots,
      checkWin,
      openModal,
    } = this;

    const newSlots = runSlots();

    const win = checkWin(newSlots);

    if (win) {
      openModal();
    }
  }

  checkWin(slots) {
    const {
      firstColor,
      secondColor,
      thirdColor,
    } = slots;

    return firstColor &&
      secondColor &&
      thirdColor &&
      firstColor === secondColor &&
      firstColor === thirdColor;
  }

  render() {
    const {
      handleClick,
      closeModal,
      afterOpenModal,
      state: {
        firstColor,
        secondColor,
        thirdColor,
        modalIsOpen,
      },
    } = this;

    return (
      <div className='hero'>
        <h1>FRUIT MACHINE</h1>
        <Item color={ firstColor } />
        <Item color={ secondColor } />
        <Item color={ thirdColor } />

        <button className='hero_btn' onClick={ handleClick }>
          play
        </button>

        <Modal
          isOpen={ modalIsOpen }
          onRequestClose={ closeModal }
          afterOpenModal={ afterOpenModal }
          style={ customStyles }
          contentLabel='Example Modal'
        >
          <div>YOU WON!</div>
          <br />
          <button onClick={ closeModal }>
            PLAY AGAIN
          </button>
        </Modal>

      </div>
    );
  }
}

export default App;
